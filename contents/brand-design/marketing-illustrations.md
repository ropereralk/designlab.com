---
name: Marketing illustrations
---

## Overview

<figure class="figure" role="figure" aria-label="Illustration sample">
  <img class="figure-img p-a-5" src="/img/brand/marketing-illustrations.svg" alt="GitLab illustration banner sample" role="img" />
  <figcaption class="figure-caption">Illustration banner sample</figcaption>
</figure>

Illustrations are the primary visual tool that we use throughout our marketing materials to depict GitLab’s values, features, user outcomes, and abstract concepts. The illustrations are an extension of our unique iconography system, and both reinforce our brand personality and are inspired by our values in the following ways:

- Overlapping elements with overlays, which highlight collaboration and transparency
- Intentional color application, driven by results
- Motion and dashed lines that represent efficiency and iteration

### Construction

An illustration typically accompanies copy; the two together are necessary and should balance and complement one another. An illustration should elevate the message and act as a form of brand storytelling, adding clarity and context to complex ideas.

When building illustrations, use this [template](https://drive.google.com/file/d/15DiL1BM9L9PeGpLuLMTl8YyUU03-Tlgw/view?usp=sharing) and adhere to the following guidelines to maintain visual consistency:

1. **Solid-filled shapes and Charcoal linework** are combined and balanced throughout.
1. **Shapes** add weight to the piece and create the overall composition of the illustration.
   1. A single shape can also be used as a background element to ground the visual. In this case, the illustration should break out of the container.
1. **Lines** are used for key areas of focus in the piece.
   1. Lines are 2px in weight.
1. **Detail** should be minimal, but added for context.
   1. Secondary lines that depict motion use Purple 04s.
1. **Backgrounds** are typically white or a solid shape filled with Purple 02s.
   1. If working with a dark background, the illustration should be placed on a gradient-filled circle that provides ample contrast for the illustration.
   1. The background shape should have two corners with a border radius of 150px.
1. **Corners** use a mix of angles and curves. Rounded corners are used sparingly as a small addition of personality; the radius should be consistent throughout the illustration.
1. **Color** is used intentionally, with white and purples from the [secondary palette](https://drive.google.com/file/d/1kCcvxYMKPkDCEFQd6imQcHhFGC14Hgte/view?usp=sharing) making up the majority of the composition. The remaining colors in that palette are used as highlights and accents to differentiate objects within the illustration.

<figure class="figure" role="figure" aria-label="Illustration sample">
  <img class="figure-img p-a-5 img-50" src="/img/brand/illustrations.svg" alt="GitLab illustration sample" role="img" />
  <figcaption class="figure-caption">Illustration sample</figcaption>
</figure>

## Marketing icons

Marketing icons are the building blocks of our illustration style and are used as singular units. They should be straightforward, only visualizing a single topic or call to action. Icons are quickly identifiable, representing common items in a redacted way. They take a secondary role to the copy and are used at small scales ranging from 24px - 216px; anything larger should default to an illustration and anything intended for functional use within the product should use the [Product icon set](/product-foundations/iconography) instead.

### Construction

The marketing icons are created using the 8px grid system found within this [template](https://drive.google.com/file/d/1V-FdsDeYcx_yPBMI9cjWclWAz_TPYqFU/view?usp=sharing). They consist entirely of linework, using a weight of 2px (1px for icons smaller than 64px) and Charcoal color. Alternatively, white is used if the icon is placed on a dark background. The icons should contain one rounded corner, if possible with the shape, with a radius of 12px (3px for icons smaller than 64px).

### Software development lifecycle

[This specific set of icons](https://drive.google.com/drive/folders/1AkJxuyB8kIGvV7e0dH6sbq5alLshuG-i?usp=sharing) represents each stage of our software development lifecycle at each stage of the process. They are designed to be presented as a set when referring to the entire journey of the lifecycle or individually when referring to a specific stage.

<figure class="figure" role="figure" aria-label="Software development lifecycle icon set">
  <img class="figure-img p-a-5" src="/img/brand/sdlc-icon-set.svg" alt="10 icons, one for each development stage" role="img" />
  <figcaption class="figure-caption">Software development lifecycle icon set</figcaption>
</figure>
